import re
from config import *


class Escola(db.Model):
    # atributos da Classe pessoa que serve apenas para herança
    id = db.Column(db.Integer, primary_key=True)
    escola = db.Column(db.String(254))
    cep = db.Column(db.String(254))
    numero = db.Column(db.Integer)
    telefone = db.Column(db.Integer)
    """def json():
        return {
            "id": self.id,
            "escola": self.escola,
            "cep": self.cep,
            "numero": self.numero,
            "telefone": self.telefone,
        }"""


class Pessoa(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    idade = db.Column(db.Integer)
    email = db.Column(db.String(254))
    telefone = db.Column(db.String(20))
    identidade = db.Column(db.Text)
    cargo = db.Column(db.String(50))
    data = db.Column(db.Date)

    escolaId = db.Column(db.Integer, db.ForeignKey(Escola.id))
    escola = db.relationship("Escola")

    __mapper_args__ = {
        'polymorphic_identity': 'pessoa',
        'polymorphic_on': cargo
    }

    def __str__(self):
        return self.nome + "[id="+str(self.id) + "], " +\
            str(self.idade) + "," + self.email + "," + self.telefone +\
            ", " + self.identidade + ", " + f'{self.data.day}/{self.data.month}/{self.data.year}' +\
            ", " + self.escola.escola + ", " + self.cargo

    def json(self):
        return {
            "id": self.id,
            "nome": self.nome,
            "idade": self.idade,
            "email": self.email,
            "telefone": self.telefone,
            "identidade": self.identidade,
            "cargo": self.cargo,
            "data": f'{self.data.day}/{self.data.month}/{self.data.year}',
            "escola": self.escola.escola
        }


class Estudante(Pessoa):
    id = db.Column(db.Integer, db.ForeignKey('pessoa.id'), primary_key=True)
    matricula = db.Column(db.String(254))
    senha = db.Column(db.String(254))
    __mapper_args__ = {
        'polymorphic_identity': 'estudante',
    }

    def __str__(self):
        return super().__str__() + f", {self.matricula},{self.senha}"

    def json(self):
        return dict(super().json(), **{"matricula": self.matricula,
                                      "senha": self.senha
                                      })


class Funcionario(Pessoa):
    id = db.Column(db.Integer, db.ForeignKey('pessoa.id'), primary_key=True)
    salario = db.Column(db.Integer)
    cargaH = db.Column(db.Integer)
    cargos = db.Column(db.String(254))
    __mapper_args__ = {
        'polymorphic_identity': cargos,
    }

    def __str__(self):
        return super().__str__() + f", {self.salario}, {self.cargaH}"

    def json(self):
        return dict(super().json(), **{"salario": self.salario,
                                      "cargaH": self.cargaH
                                      })


class Autenticado(Funcionario):
    senha = db.Column(db.String(254))

    def __str__(self):
        return super().__str__() + f",{self.senha}"

    def json(self):
        return dict(super().json(), **{"senha": self.senha})


class Administrador(Pessoa):
    id = db.Column(db.Integer, db.ForeignKey('pessoa.id'), primary_key=True)
    senha = db.Column(db.String(254))

    __mapper_args__ = {
        'polymorphic_identity': 'administrador',
    }

    def __str__(self):
        return super().__str__() + f",{self.senha}"

    def json(self):
        return dict(super().json(), **{"senha": self.senha})

for a in db.session.query(Administrador).all():
    print (a.json())
"""db.create_all()
escola = Escola(escola='aa', cep='123', numero=111, telefone=11)
joao = Administrador(nome="João da Silva", idade=1, # omitindo o telefone
            email="josilva@gmail.com",telefone="12334",identidade='aaaa', senha='123',data= date(2014, 1, 25), escolaId=1)
db.session.add(escola)
db.session.add(joao)
db.session.commit()"""
